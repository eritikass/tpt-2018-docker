```sh
# get docker machine ip
docker-machine ip

# pull new image
docker pull registry.gitlab.com/eritikass/tpt-2018-docker

# run docker image
docker run --rm -p "80:80" registry.gitlab.com/eritikass/tpt-2018-docker
```